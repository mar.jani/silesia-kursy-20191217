package pl.silesiakursy.init;

import pl.silesiakursy.model.Product;
import pl.silesiakursy.db.ProductList;

public class ProductInit {
    public void addProductToProductList(){
        ProductList.addProductToList(new Product(1,"Cukier", 1.1f));
        ProductList.addProductToList(new Product(2,"Sól", 0.99f));
        ProductList.addProductToList(new Product(3,"Mąka", 2.0f));
    }
}
