package pl.silesiakursy.init;

import pl.silesiakursy.db.RegisteredUserList;
import pl.silesiakursy.model.User;

public class UserInit {
    public void addUserToLoginUserList(){
        RegisteredUserList.addUserToRegisteredList(new User("m@m.com","123", "aaa"));
        RegisteredUserList.addUserToRegisteredList(new User("m@m.com","1234", "Beata"));
        RegisteredUserList.addUserToRegisteredList(new User("m@m.com","1234", "Adam3"));
    }
}
