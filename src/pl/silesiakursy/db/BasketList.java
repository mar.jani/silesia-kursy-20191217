package pl.silesiakursy.db;

import pl.silesiakursy.model.Basket;
import pl.silesiakursy.model.ProductBasket;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BasketList {
    private static List<Basket> basketList = new ArrayList<>();

    public static void addBasketToBasketList(Basket basket) {
        basketList.add(basket);
    }

    public static Basket findBasketByUUID(UUID uuid) {
//        for (Basket basket : basketList) {
//            if(basket.getUserUUID().equals(uuid)){
//                return basket;
//            }
//        }
//        return null;
        return basketList.stream()
                .filter(basket -> basket.getUserUUID().equals(uuid))
                .findAny()
                .orElse(null);
    }

    public static List<ProductBasket> getProductBasketListByUUIDFromBasket(UUID uuid) {
        return findBasketByUUID(uuid).getProductBasketList();
    }


}
