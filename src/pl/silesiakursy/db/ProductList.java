package pl.silesiakursy.db;

import pl.silesiakursy.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductList {
    private static List<Product> productList = new ArrayList<>();

    public static void addProductToList(Product product) {
        productList.add(product);
    }

    public static List<Product> getProductList() {
        return productList;
    }

    public static Product findProdactById(int id) {
//        for (Product product : productList) {
//            if (product.getId() == id) {
//                return product;
//            }
//        }
//        return null;
        return productList.stream()
                .filter(product -> product.getId() == id)
                .findAny()
                .orElse(null);
    }
}
