package pl.silesiakursy.db;

import pl.silesiakursy.model.User;

import java.util.ArrayList;
import java.util.List;

public class RegisteredUserList {
    private static List<User> registeredUsers = new ArrayList<>();

    public static List<User> getRegisteredUsers() {
        return registeredUsers;
    }

    public static void addUserToRegisteredList(User user) {
        registeredUsers.add(user);
    }

    public static void removeUserFromRegisteredList(User user) {
        registeredUsers.remove(user);
    }

}
