package pl.silesiakursy.db;

import pl.silesiakursy.model.User;

import java.util.ArrayList;
import java.util.List;

public class LoginedUserList {
    private static List<User> loginedUsers = new ArrayList<>();

    public static List<User> getLoginedUsers() {
        return loginedUsers;
    }

    public static void addUserToLoginList(User user) {
        loginedUsers.add(user);
    }

    public static void removeUserFromLoginList(User user) {
        loginedUsers.remove(user);
    }

}
