package pl.silesiakursy;

import pl.silesiakursy.init.ProductInit;
import pl.silesiakursy.init.UserInit;
import pl.silesiakursy.menu.RegistationAndLoginMenu;

public class Main {
    public static void main(String[] args) {
        UserInit userInit = new UserInit();
        userInit.addUserToLoginUserList();

        ProductInit productInit = new ProductInit();
        productInit.addProductToProductList();

        RegistationAndLoginMenu registationAndLoginMenu = new RegistationAndLoginMenu();
        registationAndLoginMenu.menu();
    }
}
