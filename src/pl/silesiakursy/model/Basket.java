package pl.silesiakursy.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Basket {
    private UUID userUUID;
    private List<ProductBasket> productBasketList = new ArrayList<>();

    public Basket(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public List<ProductBasket> getProductBasketList() {
        return productBasketList;
    }

    public void setProductBasketList(List<ProductBasket> productBasketList) {
        this.productBasketList = productBasketList;
    }

    public  void  addProductBasketToBasket(ProductBasket productBasket){
        productBasketList.add(productBasket);
    }
}
