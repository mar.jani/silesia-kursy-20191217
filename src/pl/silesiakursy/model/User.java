package pl.silesiakursy.model;

import java.util.UUID;

public class User {
    private UUID uuid;
    private String email;
    private String password;
    private String name;

    public User() {
    }

    public User(String email, String password, String name) {
        this.uuid = UUID.randomUUID();
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User: " +
                "email= " + email + " , " +
                "password= " + password + " , " +
                "name= " + name + ", " +
                "UUID= " + uuid + "\n";
    }
}
