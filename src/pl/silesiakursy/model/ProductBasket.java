package pl.silesiakursy.model;

public class ProductBasket {
    private int count;
    private Product product;

    public ProductBasket(int count, Product product) {
        this.count = count;
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ProductBasket{" +
                "count=" + count +
                ", product=" + product +
                '}';
    }
}
