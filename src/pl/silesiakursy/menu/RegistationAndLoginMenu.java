package pl.silesiakursy.menu;

import pl.silesiakursy.db.BasketList;
import pl.silesiakursy.model.Basket;
import pl.silesiakursy.model.User;
import pl.silesiakursy.db.LoginedUserList;
import pl.silesiakursy.db.RegisteredUserList;

import java.util.Scanner;

public class RegistationAndLoginMenu {
    public void menu() {
        System.out.println("-----MENU------");
        System.out.println("1. Rejestracja");
        System.out.println("2. Logowanie");
        System.out.println("0. Zakończ");
        System.out.println("Wybierz pozycję z menu: ");

        Scanner scanner = new Scanner(System.in);
        int menuNumber = 0;
        try {
            menuNumber = scanner.nextInt();
            scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (menuNumber == 1) {
            register();
            menu();
        } else if (menuNumber == 2) {
            login();
        } else if (menuNumber == 0) {
            System.out.println("Do zobaczenia");
        } else {
            System.out.println("Podano błędną wartość \nWybierz ponownie");
            menu();
        }
    }

    private void register() {
        Scanner scanner = new Scanner(System.in);

        String email = null;
        String password = null;
        String name = null;
        try {
            System.out.println("Podaj adres e-mail");
            email = scanner.nextLine();
            System.out.println("Podaj hasło");
            password = scanner.nextLine();
            System.out.println("Podaj imię");
            name = scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        User user = new User(email, password, name);
        RegisteredUserList.addUserToRegisteredList(user);
    }

    private void login() {
        Scanner scanner = new Scanner(System.in);

        String name = null;
        String password = null;
        try {
            System.out.println("Podaj imię");
            name = scanner.nextLine();
            System.out.println("Podaj hasło");
            password = scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean logged = false;
        for (User user : RegisteredUserList.getRegisteredUsers()) {
            if (name.equals(user.getName()) && password.equals(user.getPassword())) {
                logged = true;
                LoginedUserList.addUserToLoginList(user);

                Basket basket = new Basket(user.getUuid());
                BasketList.addBasketToBasketList(basket);

                ShopMenu shopMenu = new ShopMenu(user);
                shopMenu.menuShop();
            }
        }
        if (!logged) {
            System.out.println("Nie znaleziono użytkownika lub podano błędne hasło");
        }
    }
}
