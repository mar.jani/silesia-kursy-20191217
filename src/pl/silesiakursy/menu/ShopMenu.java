package pl.silesiakursy.menu;

import pl.silesiakursy.db.BasketList;
import pl.silesiakursy.model.Basket;
import pl.silesiakursy.model.Product;
import pl.silesiakursy.db.ProductList;
import pl.silesiakursy.model.ProductBasket;
import pl.silesiakursy.model.User;

import java.util.List;
import java.util.Scanner;

public class ShopMenu {
    private User user;

    public ShopMenu(User user) {
        this.user = user;
    }

    public void menuShop() {
        System.out.println("-----MENU------");
        System.out.println("1. Pokaż produkty");
        System.out.println("2. Dodaj nowy produkt");
        System.out.println("3. Dodaj produkt do koszyka");
        System.out.println("4. Pokaż zawartość koszyka");
        System.out.println("0. Wróć do poprzedniego menu");
        System.out.println("Wybierz pozycję z menu: ");

        Scanner scanner = new Scanner(System.in);
        int menuNumber = 0;
        try {
            menuNumber = scanner.nextInt();
            scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (menuNumber == 1) {
            showProductsList();
            menuShop();
        } else if (menuNumber == 2) {
            addProduct();
            menuShop();
        } else if (menuNumber == 3) {
            addProductToBasket();
            menuShop();
        } else if (menuNumber == 4) {
            showBasket();
            menuShop();
        } else if (menuNumber == 0) {
            RegistationAndLoginMenu registationAndLoginMenu = new RegistationAndLoginMenu();
            registationAndLoginMenu.menu();
        } else {
            System.out.println("Podano błędną wartość \nWybierz ponownie");
            menuShop();
        }
    }

    private void addProduct() {
        Scanner scanner = new Scanner(System.in);

        int id = 0;
        String name = null;
        float price = 0.0f;
        try {
            System.out.println("Podaj ID produktu:");
            id = scanner.nextInt();
            scanner.nextLine();
            System.out.println("Podaj nazwę produktu:");
            name = scanner.nextLine();
            scanner.nextLine();
            System.out.println("Podaj cenę produktu:");
            price = scanner.nextFloat();
            scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ProductList.findProdactById(id) != null) {
            System.out.println("Podane ID już istnieje");
        } else {
            Product product = new Product();
            product.setId(id);
            product.setName(name);
            product.setPrice(price);

            ProductList.addProductToList(product);
        }
    }

    private void showProductsList() {
        for (Product product : ProductList.getProductList()) {
            System.out.println(product.toString());
        }
    }

    private void addProductToBasket() {
        Scanner scanner = new Scanner(System.in);

        int id = 0;
        int count = 0;
        try {
            System.out.println("Podaj ID produktu:");
            id = scanner.nextInt();
            scanner.nextLine();
            System.out.println("Podaj ilość produktu:");
            count = scanner.nextInt();
            scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ProductList.findProdactById(id) != null) {
            Basket basket = BasketList.findBasketByUUID(user.getUuid());
            ProductBasket productBasket = new ProductBasket(count, ProductList.findProdactById(id));
            basket.addProductBasketToBasket(productBasket);
        }
    }

    private void showBasket() {
        List<ProductBasket> productBasketList = BasketList.getProductBasketListByUUIDFromBasket(user.getUuid());
        if (productBasketList.isEmpty()) {
            System.out.println("Koszyk jest pusty");
        } else {
            System.out.println(productBasketList.toString());
        }
    }
}
